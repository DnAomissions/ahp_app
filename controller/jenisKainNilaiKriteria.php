<?php

if(isset($_POST['add']))
{
    $request = getRequest();
    
    foreach($request['kriteria_id'] as $key => $kriteria)
    {
        $model = new JenisKainNilaiKriteria();
        $req = [
            'jenis_kain_id' => $request['jenis_kain_id'],
            'kriteria_id' => $kriteria,
            'sub_kriteria_id' => $request['sub_kriteria_id'][$key]
        ];
        $jenis_kain_nilai_kriteria_id = $model->select('WHERE kriteria_id='.$kriteria.' AND jenis_kain_id='.$request['jenis_kain_id'])[0]['jenis_kain_nilai_kriteria_id'];
        $model = $model->update($jenis_kain_nilai_kriteria_id, $req);
    }

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil Nilai Jenis Kain!');
    }

}

?>