<?php

if(isset($_POST['edit']))
{
    $request = getRequest();
    $kriteria_id1 = $_POST['kriteria_id1'];
    $kriteria_id2 = $_POST['kriteria_id2'];
    $model = new NilaiBobotKriteria();
    $nilai_bobot_kriteria_id = $model->select('WHERE kriteria_id1='.$kriteria_id1.' AND kriteria_id2='.$kriteria_id2)[0]['nilai_bobot_kriteria_id'];
    $model = $model->update($nilai_bobot_kriteria_id, $request);
    
    if(number_format($_POST['bobot'],2) >= 1){
        $bobot = 1/number_format($_POST['bobot'],2);
    }else{
        switch (number_format($_POST['bobot'],2)) {
            case 0.11:
                $bobot = 9;
                break;
            case 0.13:
                $bobot = 8;
                break;
            case 0.14:
                $bobot = 7;
                break;
            case 0.17:
                $bobot = 6;
                break;
            case 0.2:
                $bobot = 5;
                break;
            case 0.25:
                $bobot = 4;
                break;
            case 0.33:
                $bobot = 3;
                break;
            case 0.5:
                $bobot = 2;
                break;
            default:
                break;
        }
    }
    $request2 = [
        'kriteria_id1' => $kriteria_id2,
        'kriteria_id2' => $kriteria_id1,
        'bobot' => $bobot,
    ];

    $model = new NilaiBobotKriteria();
    $nilai_bobot_kriteria_id = $model->select('WHERE kriteria_id1='.$kriteria_id2.' AND kriteria_id2='.$kriteria_id1)[0]['nilai_bobot_kriteria_id'];
    $model = $model->update($nilai_bobot_kriteria_id, $request2);

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil edit Nilai Bobot Kriteria!');
    }else{
        $session->setSession('warning', 'Gagal edit Nilai Bobot Kriteria!');
    }
}

if(isset($_POST['edit-batch-spk']))
{
    $request = getRequest();
    foreach ($request['kriteria_id1'] as $key => $kriteria) {
        
        $kriteria_id1 = $kriteria;
        $kriteria_id2 = $request['kriteria_id2'][$key];
        if(is_array($kriteria_id1) || is_array($kriteria_id2))
        {
            echo 'WHERE kriteria_id1='.$kriteria_id1.' AND kriteria_id2='.$kriteria_id2;
            die();
        }
        $requestBobot = $request['bobot'][$key];
        $model = new NilaiBobotKriteria();
        
        $nilai_bobot_kriteria_id = $model->select('WHERE kriteria_id1='.$kriteria_id1.' AND kriteria_id2='.$kriteria_id2)[0]['nilai_bobot_kriteria_id'];
        $request1 = [
            'kriteria_id1' => $kriteria_id1,
            'kriteria_id2' => $kriteria_id2,
            'bobot' => $requestBobot,
        ];
        $model = $model->update($nilai_bobot_kriteria_id, $request1);
        if(number_format($requestBobot,2) >= 1){
            $bobot = 1/number_format($requestBobot,2);
        }else{
            switch (number_format($requestBobot,2)) {
                case 0.11:
                    $bobot = 9;
                    break;
                case 0.13:
                    $bobot = 8;
                    break;
                case 0.14:
                    $bobot = 7;
                    break;
                case 0.17:
                    $bobot = 6;
                    break;
                case 0.2:
                    $bobot = 5;
                    break;
                case 0.25:
                    $bobot = 4;
                    break;
                case 0.33:
                    $bobot = 3;
                    break;
                case 0.5:
                    $bobot = 2;
                    break;
                default:
                    break;
            }
        }
        $request2 = [
            'kriteria_id1' => $kriteria_id2,
            'kriteria_id2' => $kriteria_id1,
            'bobot' => $bobot,
        ];
    
        $model = new NilaiBobotKriteria();
        $nilai_bobot_kriteria_id = $model->select('WHERE kriteria_id1='.$kriteria_id2.' AND kriteria_id2='.$kriteria_id1)[0]['nilai_bobot_kriteria_id'];
        $model = $model->update($nilai_bobot_kriteria_id, $request2);
    
        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil edit Nilai Bobot Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal edit Nilai Bobot Kriteria!');
        }
    }

    $kriteria = new Kriteria();
    $kriterias = $kriteria->select();
    $model = new NilaiBobotKriteria();
    if($model->selectKonsistensi($kriterias))
    {
        $model->saveNilaiPrioritas();
    }else{
        $session->setSession('warning', 'Harap Ulangi Pemberian Bobot Kriteria!');
    }
}

if(isset($_POST['save']))
{
    $request = getRequest();
    
    $model = new NilaiBobotKriteria();

    $model = $model->saveNilaiPrioritas();

    if(!empty($model))
    {
        $session->setSession('success', 'Berhasil Simpan Nilai Prioritas Kriteria!');
    }else{
        $session->setSession('warning', 'Gagal Simpan Nilai Prioritas Kriteria!');
    }
}
?>