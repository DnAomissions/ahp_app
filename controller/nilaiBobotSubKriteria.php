<?php

if(isset($_POST['edit']))
{
    $request = getRequest();
    if(!isset($_POST['kriteria_id'])){
        $session->setSession('warning', 'Kriteria ID tidak teridentifikasi!');
    }else{
        $sub_kriteria_id1 = $_POST['sub_kriteria_id1'];
        $sub_kriteria_id2 = $_POST['sub_kriteria_id2'];
        $model = new NilaiBobotSubKriteria();
        $nilai_bobot_sub_kriteria_id = $model->select('WHERE sub_kriteria_id1='.$sub_kriteria_id1.' AND sub_kriteria_id2='.$sub_kriteria_id2)[0]['nilai_bobot_sub_kriteria_id'];
        $model = $model->update($nilai_bobot_sub_kriteria_id, $request);
        
        if(number_format($_POST['bobot'],2) >= 1){
            $bobot = 1/number_format($_POST['bobot'],2);
        }else{
            switch (number_format($_POST['bobot'],2)) {
                case 0.11:
                    $bobot = 9;
                    break;
                case 0.13:
                    $bobot = 8;
                    break;
                case 0.14:
                    $bobot = 7;
                    break;
                case 0.17:
                    $bobot = 6;
                    break;
                case 0.2:
                    $bobot = 5;
                    break;
                case 0.25:
                    $bobot = 4;
                    break;
                case 0.33:
                    $bobot = 3;
                    break;
                case 0.5:
                    $bobot = 2;
                    break;
                default:
                    break;
            }
        }
        $request2 = [
            'sub_kriteria_id1' => $sub_kriteria_id2,
            'sub_kriteria_id2' => $sub_kriteria_id1,
            'bobot' => $bobot,
        ];

        $model = new NilaiBobotSubKriteria();
        $nilai_bobot_sub_kriteria_id = $model->select('WHERE sub_kriteria_id1='.$sub_kriteria_id2.' AND sub_kriteria_id2='.$sub_kriteria_id1)[0]['nilai_bobot_sub_kriteria_id'];
        $model = $model->update($nilai_bobot_sub_kriteria_id, $request2);

        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil edit Nilai Bobot Sub Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal edit Nilai Bobot Sub Kriteria!');
        }
    }
}

if(isset($_POST['save']))
{
    $request = getRequest();
    if(!isset($_POST['kriteria_id'])){
        $session->setSession('warning', 'Kriteria ID tidak teridentifikasi!');
    }else{
        $model = new NilaiBobotSubKriteria();
        $kriteria_id = $_POST['kriteria_id'];

        $model = $model->saveNilaiPrioritas($kriteria_id);

        if(!empty($model))
        {
            $session->setSession('success', 'Berhasil Simpan Nilai Prioritas Sub Kriteria!');
        }else{
            $session->setSession('warning', 'Gagal Simpan Nilai Prioritas Sub Kriteria!');
        }
    }
}
?>