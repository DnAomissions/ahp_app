<?php
require_once 'Model.php';

class JenisKain extends Model
{
    public $name = 'Jenis Kain';
    public $table = 'jenis_kain';
    public $primaryKey = 'jenis_kain_id';
    protected $columns = ['jenis_kain', 'keterangan', 'hasil_prioritas', 'ranking'];
    protected $hide = [];
    protected $many2one = [];
    protected $one2many = [];

    function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->initialRelation();
    }

    function initialRelation()
    {
        $this->one2many[] = [
            'class' => new JenisKainNilaiKriteria(),
            'fkey' => 'jenis_kain_id',
            'pkey' => 'jenis_kain_id'
        ];
    }

    /**
     * Custom Function
     */
    
    public function hitungRanking()
    {
        $kriteria = new Kriteria();
        $jenis_kain_nilai_kriteria = new JenisKainNilaiKriteria();
        
        $jenis_kains = $this->select();
        $kriterias = $kriteria->select();        

        foreach($jenis_kains as $jenis_kain)
        {
            $jumlah = 0;
            foreach($kriterias as $kriteria)
            {
                $jumlah += $jenis_kain_nilai_kriteria->selectPrioritasNilaiAlternatif($jenis_kain['jenis_kain_id'], $kriteria);
            }

            $this->update($jenis_kain['jenis_kain_id'], ["hasil_prioritas" => $jumlah]);
        }

        $jenis_kains = $this->select("ORDER BY hasil_prioritas DESC");
        $rank = 1;
        foreach($jenis_kains as $jenis_kain)
        {
            $this->update($jenis_kain['jenis_kain_id'], ["ranking" => $rank]);
            $rank++;
        }
    }

    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            foreach($this->many2one as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['pkey'].'='.$row[$j['fkey']])[0];
            }
            foreach($this->one2many as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['fkey'].'='.$row[$this->primaryKey]);
            }
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        $jenisKainNilaiKriteria = new JenisKainNilaiKriteria();
        $jenisKainNilaiKriteria->generateDefaultNilaiKriteria($data[0]['jenis_kain_id']);
		return $data[0];
    }
    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>