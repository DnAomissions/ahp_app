<?php
require_once 'Model.php';

class JenisKainNilaiKriteria extends Model
{
    public $name = 'Jenis Kain Nilai Kriteria';
    public $table = 'jenis_kain_nilai_kriteria';
    public $primaryKey = 'jenis_kain_nilai_kriteria_id';
    protected $columns = ['jenis_kain_nilai_kriteria_id', 'jenis_kain_id', 'kriteria_id', 'sub_kriteria_id'];
    protected $hide = [];
    protected $many2one = [];
    protected $one2many = [];

    function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->initialRelation();
    }

    function initialRelation()
    {
        $this->many2one[] = [
            'class' => new Kriteria(),
            'fkey' => 'kriteria_id',
            'pkey' => 'kriteria_id'
        ];
        $this->many2one[] = [
            'class' => new SubKriteria(),
            'fkey' => 'sub_kriteria_id',
            'pkey' => 'sub_kriteria_id'
        ];
    }

    /**
     * Custom Function
     */
    
    public function generateDefaultNilaiKriteria($jenis_kain_id)
    {
        $kriteria = new Kriteria();
        $kriterias = $kriteria->select();
        
        $res = [];

        foreach($kriterias as $key => $kriteria)
        {
            $value = [
                'jenis_kain_id' => $jenis_kain_id,
                'kriteria_id' => $kriteria['kriteria_id'],
                'sub_kriteria_id' => $kriteria['sub_kriteria'][0]['sub_kriteria_id']
            ];

            $select = $this->select('WHERE jenis_kain_id='.$jenis_kain_id.' AND kriteria_id='.$kriteria['kriteria_id']);
            
            if(count($select) <= 0)
            {
                $res[] = $this->create($value);
            }
        }
    }

    public function selectPrioritasNilaiAlternatif($jenis_kain_id, $kriteria)
    {
        $nilai_kriteria = $this->select('WHERE jenis_kain_id='.$jenis_kain_id.' AND kriteria_id='.$kriteria['kriteria_id'])[0];

        return number_format($nilai_kriteria['sub_kriteria']['nilai_prioritas'] * $nilai_kriteria['kriteria']['nilai_prioritas'], 2);
    }
    
    /**
     * Basic Function
     */
    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            foreach($this->many2one as $j)
            {
            $select = $j['class']->select('WHERE '.$j['pkey'].'='.$row[$j['fkey']]);
            if(count($select) > 0)
            {
            $row[$j['class']->table] = $select[0];
            }
            }
            foreach($this->one2many as $j)
            {
                $row[$j['class']->table] = $j['class']->select('WHERE '.$j['fkey'].'='.$row[$this->primaryKey]);
            }
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>