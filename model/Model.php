<?php
require_once ROOT.'/config/DB.php';

abstract class Model
{
    public $db;
    public $table;
    public $primaryKey = 'id';
    protected $columns = [];
    protected $hide = [];
    protected $timestamps = true;

    function __construct(array $attributes = [])
    {
        $db = new DB();
        $this->db = $db->mysqli;
    }

    abstract public function select($conditions = null);

    abstract public function find($id);

    abstract public function create($array);

    abstract public function update($id, $array);

    abstract public function delete($id);

    function sessionError($message)
    {
        $_SESSION['error'] = $message;
    }
}
?>