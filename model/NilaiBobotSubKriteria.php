<?php
require_once 'Model.php';
require_once 'SubKriteria.php';

class NilaiBobotSubKriteria extends Model
{
    public $name = 'Nilai Bobot Sub Kriteria';
    public $table = 'nilai_bobot_sub_kriteria';
    public $primaryKey = 'nilai_bobot_sub_kriteria_id';
    protected $columns = ['sub_kriteria_id1', 'bobot', 'sub_kriteria_id2'];
    protected $hide = [];

    /**
     * Custom Function
     */

    public function generateDefaultBobot($kriteria_id)
    {
        $sub_kriteria = new SubKriteria();
        $sub_kriterias = $sub_kriteria->select('WHERE kriteria_id='.$kriteria_id);
        
        $res = [];

        foreach($sub_kriterias as $key1 => $sub_kriteria1)
        {
            foreach($sub_kriterias as $key2 => $sub_kriteria2)
            {
                $value = [
                    'sub_kriteria_id1' => $sub_kriteria1["sub_kriteria_id"],
                    'sub_kriteria_id2' => $sub_kriteria2["sub_kriteria_id"],
                    'bobot' => 1
                ];

                $select = $this->select('WHERE sub_kriteria_id1='.$sub_kriteria1["sub_kriteria_id"].' AND sub_kriteria_id2='.$sub_kriteria2["sub_kriteria_id"]);
                
                if(count($select) <= 0)
                {
                    $res[] = $this->create($value);
                }
            }
        }
    }

    public function selectBobot($sub_kriteria_id1, $sub_kriteria_id2)
    {
        $res = $this->select('WHERE sub_kriteria_id1='.$sub_kriteria_id1.' AND sub_kriteria_id2='.$sub_kriteria_id2);

        return $res[0]['bobot'];
    }

    public function selectJumlah($sub_kriteria_id)
    {
        $sub_kriteria = new SubKriteria();
        $sub_kriterias = $sub_kriteria->select();
        
        foreach ($sub_kriterias as $key => $sub_kriteria) {
            $sub_kriteria_ids[] = $sub_kriteria['sub_kriteria_id'];
        }

        $query = 'SELECT SUM(bobot) AS "jumlah" FROM '.$this->table.' WHERE sub_kriteria_id2='.$sub_kriteria_id.' AND sub_kriteria_id1 in ('.implode(',',$sub_kriteria_ids).')';

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res[0]['jumlah'];
    }

    public function selectNormalisasi($sub_kriteria1, $sub_kriteria2)
    {
        return number_format($this->selectBobot($sub_kriteria1, $sub_kriteria2)/$this->selectJumlah($sub_kriteria2), 2);
    }

    public function selectJumlahNormalisasi($sub_kriteria_id, $sub_kriterias)
    {
        $jumlah = 0;
        foreach($sub_kriterias as $sub_kriteria)
        {
            $jumlah += $this->selectNormalisasi($sub_kriteria_id, $sub_kriteria['sub_kriteria_id']);
        }

        return number_format($jumlah, 2);
    }

    public function selectPrioritas($sub_kriteria_id, $sub_kriterias)
    {
        return number_format($this->selectJumlahNormalisasi($sub_kriteria_id, $sub_kriterias)/count($sub_kriterias), 2);
    }

    public function selectPrioritasSubKriteria($sub_kriteria_id, $sub_kriterias)
    {
        $prioritas = [];
        foreach($sub_kriterias as $sub_kriteria)
        {
            $prioritas[] = $this->selectPrioritas($sub_kriteria['sub_kriteria_id'], $sub_kriterias);
        }
        return number_format($this->selectPrioritas($sub_kriteria_id, $sub_kriterias)/max($prioritas), 2);
    }

    public function selectMatriksPenjumlahan($sub_kriteria_id1, $sub_kriteria_id2, $sub_kriterias)
    {
        return number_format($this->selectBobot($sub_kriteria_id1, $sub_kriteria_id2)*$this->selectPrioritas($sub_kriteria_id2, $sub_kriterias), 2);
    }

    public function selectJumlahPenjumlahan($sub_kriteria_id, $sub_kriterias)
    {
        $jumlah = 0;
        foreach($sub_kriterias as $sub_kriteria)
        {
            $jumlah += $this->selectMatriksPenjumlahan($sub_kriteria_id, $sub_kriteria['sub_kriteria_id'], $sub_kriterias);
        }

        return number_format($jumlah, 2);
    }

    public function selectHasil($sub_kriteria_id, $sub_kriterias)
    {
        return $this->selectJumlahPenjumlahan($sub_kriteria_id, $sub_kriterias)+$this->selectPrioritas($sub_kriteria_id, $sub_kriterias);
    }

    public function selectJumlahHasil($sub_kriterias)
    {
        $jumlah = 0;
        foreach($sub_kriterias as $sub_kriteria)
        {
            $jumlah += $this->selectHasil($sub_kriteria['sub_kriteria_id'], $sub_kriterias);
        }

        return number_format($jumlah, 2);
    }

    public function selectLambdaMaks($sub_kriterias)
    {
        return number_format($this->selectJumlahHasil($sub_kriterias)/count($sub_kriterias), 2);
    }

    public function selectCI($sub_kriterias)
    {
        return number_format(($this->selectLambdaMaks($sub_kriterias)-count($sub_kriterias))/(count($sub_kriterias)-1), 2);
    }

    public function selectCR($sub_kriterias)
    {
        $ratioIndex = [
            0,
            0,
            0.58,
            0.9,
            1.12,
            1.24,
            1.32,
            1.41,
            1.46,
            1.49
        ];

        return number_format($this->selectCI($sub_kriterias)/$ratioIndex[count($sub_kriterias)-1], 2);
    }

    public function selectKonsistensi($sub_kriterias)
    {
        return ($this->selectCR($sub_kriterias) <= 0.1);
    }

    public function saveNilaiPrioritas($kriteria_id)
    {
        $sub_kriteria = new SubKriteria();
        $sub_kriterias = $sub_kriteria->select('WHERE kriteria_id='.$kriteria_id);
        
        $res = [];

        foreach($sub_kriterias as $sub_kriteria)
        {
            $model = new SubKriteria();
            $value = [
                'nilai_prioritas' => $this->selectPrioritasSubKriteria($sub_kriteria['sub_kriteria_id'], $sub_kriterias)
            ];

            $res[] = $model->update($sub_kriteria['sub_kriteria_id'], $value);
        }

        return $res;
    }

    /**
     * Basic Function
     */
    public function select($conditions = null)
    {
        $columns = $this->getColumns();
        $query = 'SELECT '.$this->columnsToString($columns).' FROM '.$this->table;
        
        if(!is_null($conditions))
        {
            $query = $query.' '.$conditions;
        }

        $result = $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        $res = [];
        while ($row=$result->fetch_assoc())
        {
            $res[] = $row;
        }
        
		return $res;
    }

    public function find($id)
    {
        return $this->select('WHERE '.$this->primaryKey.'='.$id)[0];
    }

    public function create($array)
    {
        $query = 'INSERT INTO '.$this->table.' SET '.$this->queryColumn($array, ', ', false);
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->queryColumn($array, ' AND '));

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }
    public function update($id, $array)
    {
        $query = 'UPDATE '.$this->table.' SET '.$this->queryColumn($array).' WHERE '.$this->primaryKey.'='.$id;
        
        $this->db->query($query); 

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }
        
        $data = $this->select('WHERE '.$this->primaryKey.'='.$id);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

		return $data[0];
    }

    public function delete($id)
    {
        $query = 'DELETE FROM '.$this->table.' WHERE '.$this->primaryKey.'='.$id;
        $this->db->query($query);

        if($this->db->error)
        {
            $this->sessionError("MySQL Error: ".$this->db->error);
            return false;
        }

        return true;
    }

    function queryColumn($array, $delimiter = ', ', $is_update = true)
    {
        foreach($this->getColumns(false) as $column)
        {
            if($is_update && $column == 'created_at')
            {
                continue;
            }
            $value = null;
            if(array_key_exists($column, $array))
            {
                $value = $array[$column];
            }
            $res[] = $this->setColumn($column, $value);
        }
        $res = array_filter($res);

        return implode($delimiter, $res);
    }

    function setColumn($key, $value = null)
    {
        if($key == 'created_at' || $key == 'updated_at')
        {
            $value = date('Y-m-d H:i:s');
        }
        if($value==null){
            return '';
        }
        if(!is_numeric($value))
        {
            return $key.'="'.$value.'"';
        }
        return $key.'='.$value;
    }

    function getColumns($primaryKey = true)
    {
        $columns = $this->columns;
        if($primaryKey)
        {
            array_unshift($columns, $this->primaryKey);
        }
        foreach($this->hide as $hide)
        {
            if (($key = array_search($hide, $columns)) !== false) {
                unset($columns[$key]);
            }
        }
        if($this->timestamps)
        {
            $columns[] = 'created_at';
            $columns[] = 'updated_at';
        }
        return $columns;
    }

    function columnsToString($columns)
    {
        return implode(', ', $columns);
    }
}
?>