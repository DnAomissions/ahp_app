<style>
.logoig {
  width: 200px;
}

.logowa {
  width: 200px;
}

.logoemail {
  width: 200px;
}
</style>

<div class="jumbotron p-3 my-3 jumbotron-fluid bg-danger text-light text-center">
  <div class="container">
    <h1 class="display-4">Contact Info</h1>
    <p class="lead">Anda bisa langsung menghubungi kami segera untuk informasi dan pemesanan, dapatkan pelayanan dan penawaran terbaik dari kami konveksi Brother Company untuk setiap produk yang akan anda pesan, tanpa harus ribet dan buang - buang waktu anda. Kepuasan anda adalah tanggung jawab kami !!!</p>
    <h4>Customer Service Online via Telp./ WA :</h4>
    <p>Bobby Rahman - 0895-3391-72203<p>
    <a target="_blank" href="https://wa.me/+62895339172203?text=Assalamualaikum%20Brother%20Company,%20ada%20yang%20mau%20saya%20tanyakan..."><img class="logowa" src="<?=url('/image/whatsapp_logo.png')?>"></a>
    <h4>Instagram :</h4>
    <p>@brother15.id<p>
    <a target="_blank" href="https://www.instagram.com/brother15.id/"><img class="logoig" src="<?=url('/image/instagram_logo.png')?>"></a> 
    <h4>Email :</h4>
    <p>brother15.company@gmail.com<p>
    <a target="_blank" href="mailto:brother15.company@gmail.com"><img class="logoemail" src="<?=url('/image/email_logo.png')?>"></a>
    <h4>Office :</h4>
    <p>Perum Griya Jatinangor 2 Blok A16 Jalan Cendana No.001 Rt02/013 Desa Cinanjung, Kecamatan Tanjungsari, Kabupaten Sumedang (45362).<p>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15843.223088829292!2d107.7961928!3d-6.9138114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7d922438d8fad629!2sCAFE%20JABAR!5e0!3m2!1sen!2sid!4v1607572260112!5m2!1sen!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
</div>