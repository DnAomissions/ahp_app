<?php
    // Form Setup
    $kriteria = new Kriteria();
    $nilaiBobotKriteria = new NilaiBobotKriteria();
    $jenis_kain = new JenisKain();
    $jenis_kain_nilai_kriteria = new JenisKainNilaiKriteria();

    $jenis_kain->hitungRanking();
    $jenis_kains = $jenis_kain->select();

    $kriterias = $kriteria->select();
    if(!isset($_POST['hitung']))
    {
        header('location:'.url('/'));
    }
    
    if($nilaiBobotKriteria->selectKonsistensi($kriterias))
    {
        $nilaiBobotKriteria->saveNilaiPrioritas();
    }
    $rekomendasis = $jenis_kain->select("WHERE ranking <= 3 ORDER BY ranking ASC");
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk')?>">SPK</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.form')?>">Form</a></li>
    <li class="breadcrumb-item active">Rekomendasi</li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <?php
            if($nilaiBobotKriteria->selectKonsistensi($kriterias))
            {
        ?>
        <div class="row p-3">
            <div class="col-12 text-center">
                <h3>Rekomendasi Kain</h3>
                <br/>
                <?php
                    foreach ($rekomendasis as $key => $rekomendasi) {
                        $ranking = $rekomendasi['ranking'];
                        if($ranking != 1) {
                ?>
                        <span class="h5 text-primary border border-primary rounded p-2">#<?=$ranking?> <?=$rekomendasi['jenis_kain']?></span>
                        <p class="mt-4"><?=$rekomendasi['keterangan']?></p>
                        <br/>
                <?php
                        } else {
                ?>
                        <br/>
                        <span class="h2 text-success border border-success rounded p-2">#<?=$ranking?> <?=$rekomendasi['jenis_kain']?></span>
                        <p class="mt-4"><?=$rekomendasi['keterangan']?></p>
                        <br/>
                <?php
                        }
                    }
                ?>
                <h4>Hubungi Kami di:</h4>
                <p>Bobby Rahman - 0895-3391-72203<p>
                <a target="_blank" href="https://wa.me/+62895339172203?text=Assalamualaikum%20Brother%20Company,%20ada%20yang%20mau%20saya%20tanyakan..."><img class="logowa" src="<?=url('/image/whatsapp_logo.png')?>"></a>
            </div>
        </div>
        <?php
            }else{
        ?>
        <div class="row p-3">
            <div class="col-12 text-center">
                <h2 class="text-danger">Harap Ulangi!</h2>
                <p>Perhitungan Tidak Konsisten. Harap ulangi perbandingan!</p>
                <a href="<?=url('?page=spk.form')?>" class="btn btn-danger">Form</a>
            </div>
        </div>
        <?php
            }
        ?>
        
        <a class="btn btn-primary" data-toggle="collapse" href="#collapsePerhitungan" role="button" aria-expanded="false" aria-controls="collapsePerhitungan">
            Lihat Perhitungan
        </a>
        <div class="collapse" id="collapsePerhitungan">
            <div class="row p-3">
                <div class="col-12">
                    <h4>Table Perbandingan Berpasangan</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Kriteria</th>
                            <?php
                                foreach($kriterias as $kriteria)
                                {
                            ?>
                                <th><?=$kriteria['kriteria']?></th>
                            <?php
                                }
                            ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($kriterias as $kriteria1)
                                {
                            ?>
                                <tr>
                                    <th><?=$kriteria1['kriteria']?></th>
                                    <?php
                                        foreach($kriterias as $kriteria2)
                                        {
                                    ?>
                                        <td class="bobot-nilai" id="bobot-<?=$kriteria1['kriteria_id']?>-<?=$kriteria2['kriteria_id']?>"><?=$nilai = number_format($nilaiBobotKriteria->selectBobot($kriteria1['kriteria_id'], $kriteria2['kriteria_id']),2)?></td>
                                    <?php
                                        }
                                    ?>
                                </tr>
                            <?php
                                }
                            ?>
                            <tr>
                                <th>Jumlah</th>
                                <?php
                                    foreach($kriterias as $kriteria)
                                    {
                                ?>
                                    <td><?=number_format($nilaiBobotKriteria->selectJumlah($kriteria['kriteria_id']),2)?></td>
                                <?php
                                    }
                                ?>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row p-3">
                <div class="col-12">
                    <h4>Matriks Nilai(Normalisasi)</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?=$kriteria['kriteria']?></th>
                            <?php
                                foreach($kriterias as $kriteria)
                                {
                            ?>
                                <th><?=$kriteria['kriteria']?></th>
                            <?php
                                }
                            ?>
                                <th>Jumlah</th>
                                <th>Prioritas</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($kriterias as $kriteria1)
                                {
                            ?>
                                <tr>
                                    <th><?=$kriteria1['kriteria']?></th>
                                    <?php
                                        foreach($kriterias as $kriteria2)
                                        {
                                    ?>
                                        <td><?=$nilaiBobotKriteria->selectNormalisasi($kriteria1['kriteria_id'], $kriteria2['kriteria_id'])?></td>
                                    <?php  
                                        }
                                    ?>
                                    <td><?=$nilaiBobotKriteria->selectJumlahNormalisasi($kriteria1['kriteria_id'], $kriterias)?></td>
                                    <td><?=$nilaiBobotKriteria->selectPrioritas($kriteria1['kriteria_id'], $kriterias)?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row p-3">
                <div class="col-12">
                    <h4>Matriks Penjumlahan Setiap Baris</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?=$kriteria['kriteria']?></th>
                            <?php
                                foreach($kriterias as $kriteria)
                                {
                            ?>
                                <th><?=$kriteria['kriteria']?></th>
                            <?php
                                }
                            ?>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($kriterias as $kriteria1)
                                {
                            ?>
                                <tr>
                                    <th><?=$kriteria1['kriteria']?></th>
                                    <?php
                                        foreach($kriterias as $kriteria2)
                                        {
                                    ?>
                                        <td><?=$nilaiBobotKriteria->selectMatriksPenjumlahan($kriteria1['kriteria_id'], $kriteria2['kriteria_id'], $kriterias)?></td>
                                    <?php  
                                        }
                                    ?>
                                    <td><?=$nilaiBobotKriteria->selectJumlahPenjumlahan($kriteria1['kriteria_id'], $kriterias)?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row p-3">
                <div class="col-12">
                    <h4>Perhitungan Rasio Konsistensi</h4>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th><?=$kriteria['kriteria']?></th>
                                <th>Jumlah</th>
                                <th>Prioritas</th>
                                <th>Hasil</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($kriterias as $kriteria1)
                                {
                            ?>
                                <tr>
                                    <th><?=$kriteria1['kriteria']?></th>
                                    <td><?=$nilaiBobotKriteria->selectJumlahPenjumlahan($kriteria1['kriteria_id'], $kriterias)?></td>
                                    <td><?=$nilaiBobotKriteria->selectPrioritas($kriteria1['kriteria_id'], $kriterias)?></td>
                                    <td><?=$nilaiBobotKriteria->selectHasil($kriteria1['kriteria_id'], $kriterias)?></td>
                                </tr>
                            <?php
                                }
                            ?>
                            <tr>
                                <th colspan="3" class="text-right">Jumlah</th>
                                <td><?=$nilaiBobotKriteria->selectJumlahHasil($kriterias)?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row p-3">
                <div class="offset-3 col-6">
                    <h4>Konsistensi</h4>
                    <table class="table table-bordered table-striped table-hover">
                        <tr>
                            <th>&lambda; Maks</th>
                            <td><?=$nilaiBobotKriteria->selectLambdaMaks($kriterias)?></td>
                        </tr>
                        <tr>
                            <th>CI</th>
                            <td><?=$nilaiBobotKriteria->selectCI($kriterias)?></td>
                        </tr>
                        <tr>
                            <th>CR</th>
                            <td><?=$nilaiBobotKriteria->selectCR($kriterias)?></td>
                        </tr>
                        <tr class="text-light <?=($nilaiBobotKriteria->selectKonsistensi($kriterias)) ? 'bg-success': 'bg-danger'?>">
                            <th colspan="2" class="text-center"><?=($nilaiBobotKriteria->selectKonsistensi($kriterias)) ? 'KONSISTEN': 'TIDAK KONSISTEN'?></th>
                        </tr>
                    </table>
                </div>
                <div class="offset-2 col-8">
                    <?php
                        if(!$nilaiBobotKriteria->selectKonsistensi($kriterias))
                        {
                    ?>
                        <div class="alert alert-warning" role="alert">
                            Hasil perhitungan Tidak Konsisten. Harap Ulangi Perbandingan!
                        </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
            <?php
            if($nilaiBobotKriteria->selectKonsistensi($kriterias))
            {
            ?>
            <div class="row p-3">
                <div class="col-12">
                    <h4>Rekomendasi</h4>
                    <table class="table table-bordered table-striped table-hover" id="tableRekomendasi">
                        <thead>
                            <tr>
                                <th>Nama Alternatif</th>
                                <?php
                                    foreach($kriterias as $kriteria)
                                    {
                                ?>
                                    <th><?=$kriteria['kriteria']?></th>
                                <?php
                                    }
                                ?>
                                <th>Hasil Prioritas</th>
                                <th>Ranking</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($jenis_kains as $index => $jenis_kain)
                                {
                            ?>
                            <tr>
                                <td><?=$jenis_kain['jenis_kain']?></td>
                                <?php
                                    $jumlah = 0;
                                    foreach($kriterias as $kriteria)
                                    {
                                ?>
                                    <td><?=$nilai = $jenis_kain_nilai_kriteria->selectPrioritasNilaiAlternatif($jenis_kain['jenis_kain_id'], $kriteria)?></td>
                                    
                                <?php
                                        $jumlah += $nilai;
                                    }
                                ?>
                                <td><?=$jumlah?></td>
                                <td><?=$jenis_kain['ranking']?></td>
                            </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
</div>

<script>
    function changeButtonBobotActive(){
        var kriteria_id1 = $('#kriteria_id1').val();
        var kriteria_id2 = $('#kriteria_id2').val();

        if(kriteria_id1 == kriteria_id2){
            $('#btn-submit-bobot').prop("disabled", true);
            $('.btn-insert-bobot').prop("disabled", true);
        }else{
            $('#btn-submit-bobot').prop("disabled", false);
            $('.btn-insert-bobot').prop("disabled", false);
        }
        $('.bobot-nilai').removeClass('bg-warning');
        $('#bobot-'+kriteria_id1+'-'+kriteria_id2).addClass('bg-warning');
        var bobot = $('#bobot-'+kriteria_id1+'-'+kriteria_id2).html();
        var index = 0;
        switch (bobot) {
            case '0.11':
                index = 16;
                break;
            case '0.13':
                index = 15;
                break;
            case '0.14':
                index = 14;
                break;
            case '0.17':
                index = 13;
                break;
            case '0.20':
                index = 12;
                break;
            case '0.25':
                index = 11;
                break;
            case '0.33':
                index = 10;
                break;
            case '0.50':
                index = 9;
                break;
            case '1.00':
                index = 8;
                break;
            case '2.00':
                index = 7;
                break;
            case '3.00':
                index = 6;
                break;
            case '4.00':
                index = 5;
                break;
            case '5.00':
                index = 4;
                break;
            case '6.00':
                index = 3;
                break;
            case '7.00':
                index = 2;
                break;
            case '8.00':
                index = 1;
                break;
            case '9.00':
                index = 0;
                break;
            default:
                break;
        }
        $('.btn-insert-bobot').removeClass('btn-primary').addClass('btn-outline-primary');
        $('#btn-bobot'+index).removeClass('btn-outline-primary').addClass('btn-primary');
    }

    $(document).ready(function(){
        changeButtonBobotActive();

        // Event Onchange Kriteria
        $('.input-kriteria_id').on('change', function(){
            
            changeButtonBobotActive();
        })

        // Event Click Button Perbandingan
        $('.btn-insert-bobot').click(function(){
            $('.btn-insert-bobot').removeClass('btn-primary').addClass('btn-outline-primary');
            $(this).removeClass('btn-outline-primary').addClass('btn-primary');
            var bobot = $(this).data('value');
            $('#bobot').val(bobot);
        })
        
        var indexLastColumn = $("#tableRekomendasi").find('tr')[0].cells.length-1;
        $('#tableRekomendasi').DataTable();
    })
</script>