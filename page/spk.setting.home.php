<style>
.tabelnp {
  width: 500px;
}
</style>
<div class="jumbotron p-3 my-3 jumbotron-fluid text-center bg-info text-light">
  <div class="container">
    <h1 class="display-4">SPK Setting</h1>
    <p class="lead">Pada menu ini, Admin bisa melakukan aksi tambah, hapus, edit dan beri nilai poin pada setiap kriteria, sub kriteria dan jenis kain yang nantinya akan menjadi data default jika konsumen mengaksesnya.
                <br>Admin dapat memberi nilai bobot poin jika data yang dimasukan melebihi 2 data. Nilai bobot poin tersebut diatur pada tabel dibawah ini.
    </p>
    <img class="tabelnp d-block ml-auto mr-auto" src="<?=url('/image/tabelnilaipoin.jpg')?>">
  </div>
</div>