<?php
    // Form Setup
    $kriteria_id = '';
    $kriteria = new Kriteria();
    $subKriteria = new SubKriteria();
    if(isset($_GET['parent_id']))
    {
        $kriteria_id = $_GET['parent_id'];
        $kriteria = $kriteria->find($kriteria_id);
        if(!$kriteria)
        {
            $session->setSession('warning', 'Kriteria ID Not Valid.');
            header('location:'.url('?page=spk.setting&subPage=kriteria'));
        }
        $subKriterias = $subKriteria->select('WHERE kriteria_id='.$kriteria_id);
    }else{
        $session->setSession('warning', 'URL Error');
        header('location:'.url('?page=spk.setting&subPage=kriteria'));
    }
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bg-white">
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting')?>">SPK Setting</a></li>
    <li class="breadcrumb-item"><a href="<?=url('?page=spk.setting&subPage=kriteria')?>">Kriteria</a></li>
    <li class="breadcrumb-item active"><?=$kriteria['kriteria']?></li>
</ol>
<!-- Content -->
<div class="card">
    <div class="card-body">
        <div class="row my-3">
            <div class="col-12">
                <form>
                    <div class="form-group row" style="display:none;">
                        <label class="col-sm-2 col-form-label" for="kriteria_id">Kriteria ID</label>
                        <div class="col-sm-10">
                            <input type="text" name="kriteria_id" value="<?=$kriteria['kriteria_id']?>" id="kriteria_id" placeholder="Kriteria ID" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="kode_kriteria">Kode Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" name="kode_kriteria" value="<?=$kriteria['kode_kriteria']?>" id="kode_kriteria" placeholder="Kode Kriteria" class="form-control" readonly/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="kriteria">Kriteria</label>
                        <div class="col-sm-10">
                            <input type="text" name="kriteria" value="<?=$kriteria['kriteria']?>" id="kriteria" placeholder="Kriteria" class="form-control" readonly/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php
        if(count($subKriterias) < 3)
        {
        ?>
        <div class="row p-3">
            <div class="col-12">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Warning!</strong> Data Sub Kriteria kurang dari 3. Harap tambah terlebih dahulu!
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php
        }
        ?>
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="btn-group" role="group">
                    <a href="<?=url('?page=sub.kriteria.form&subPage=kriteria&parent_id='.$kriteria_id)?>" class="btn btn-sm btn-outline-success">Tambah Sub Kriteria</a>
                    <?php
                        if(count($subKriterias) > 2)
                        {
                    ?>
                        <a href="<?=url('?page=sub.kriteria.perbandingan&subPage=kriteria&parent_id='.$kriteria_id)?>" class="btn btn-sm btn-outline-info">Nilai Poin Sub Kriteria</a>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="row p-3 my-3">
            <div class="col-12">
                <table class="table table-bordered table-hover" id="tableSubKriteria">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Sub Kriteria</th>
                            <th>Keterangan</th>
                            <th>Nilai Prioritas</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(count($subKriterias) > 0)
                        {
                            foreach($subKriterias as $key => $subKriteria)
                            {
                        ?>
                        <tr>
                            <td><?=$key+1?></td>
                            <td><?=$subKriteria['sub_kriteria']?></td>
                            <td><?=$subKriteria['keterangan']?></td>
                            <td><?=$subKriteria['nilai_prioritas']?></td>
                            <td>
                                <form action="?page=sub.kriteria.table&subPage=kriteria&parent_id=<?=$subKriteria['kriteria_id']?>" method="POST">
                    <input type="hidden" name="model" value="subKriteria"/>
                                    <input type="hidden" name="sub_kriteria_id" value="<?=$subKriteria['sub_kriteria_id']?>"/>
                                    <div class="btn-group" role="group">
                                        <a href="<?=url('?page=sub.kriteria.form&subPage=kriteria&parent_id='.$subKriteria['kriteria_id'].'&id='.$subKriteria['sub_kriteria_id'])?>" class="btn btn-sm btn-info">Edit</a>
                                        <button type="submit" name="delete" class="btn btn-sm btn-danger" onclick="return confirm('Hapus Sub Kriteria?')">Hapus</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#tableSubKriteria').DataTable();
    })
</script>